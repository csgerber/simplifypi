package com.simplifypi.gerber;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

//lombok @Data give us automatic getters and setters on members
@Data
public class SimplifyPiQuestion implements Serializable {
    //identify the version
    private static final long serialVersionUID = 1000001L;
    //members
    private String fileName;
    private String question;
    private String answer;

    //serializable objects require a no-arg constructor
    public SimplifyPiQuestion() {}

    public SimplifyPiQuestion(String fileName, String question, String answer) {
        this.fileName = fileName;
        this.question = question;
        this.answer = answer;
    }
}
