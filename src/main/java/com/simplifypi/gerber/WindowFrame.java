package com.simplifypi.gerber;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.Optional;

@Data
public class WindowFrame extends JFrame implements KeyListener {

    private JLabel labelQuestion, labelStatus, labelCleanDirty;
    private JPanel panelButtons, panelSouth;
    private JButton buttonClose, buttonNew, buttonSave;
    private JTextArea answerText;
    private JScrollPane areaScrollPane;
    public enum State {
        EMPTY, CLEAN, DIRTY, ERROR
    }
    //members
    private State spqState;
    private Optional<SimplifyPiQuestion> currentQuestionOptional;

    public WindowFrame() {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStateAndUpdateUserInterface(State state){
        spqState = state;

        switch (state){
            case EMPTY:
                getLabelQuestion().setEnabled(false);
                getLabelQuestion().setText(" ");
                getAnswerText().setEnabled(false);
                getButtonSave().setEnabled(false);
                break;
            case CLEAN:
            case DIRTY:
            case ERROR:
                getLabelQuestion().setEnabled(true);
                getLabelQuestion().setText(currentQuestionOptional.isPresent() ?
                        currentQuestionOptional.get().getQuestion() : "" );
                getAnswerText().setEnabled(true);
                getButtonSave().setEnabled(true);
                break;

        }
    }


    private void initialize()  {

        getContentPane().setLayout(new BorderLayout());
        labelQuestion = new JLabel();
        labelQuestion.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        getContentPane().add(labelQuestion, BorderLayout.NORTH);

        answerText = new JTextArea();
        answerText.setEditable(true);
        answerText.setLineWrap(true);
        answerText.setMargin( new Insets(2,10,2,10) );
        answerText.addKeyListener(this);

        areaScrollPane = new JScrollPane(answerText);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        areaScrollPane.setHorizontalScrollBarPolicy(
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);


        getContentPane().add(areaScrollPane, BorderLayout.CENTER);

        panelButtons = new JPanel();
        panelSouth = new JPanel();
        //instructions identify this button with both "Close" and "Quit"
        buttonClose = new JButton("Close/Quit");
        buttonClose.addActionListener(e -> {
            //if the status is DIRTY, fire a dialog with option to save or discard.
            System.exit(0);
        });
        buttonSave = new JButton("Save");
        buttonSave.addActionListener(e -> {
            SerializeMgr.getInstance().writeSpq(currentQuestionOptional.orElseThrow());
        });

        buttonNew = new JButton("New");
        buttonNew.addActionListener(e -> {

            KeyListener keyListener = new KeyListener() {
                @Override
                public void keyTyped(KeyEvent ke) {

                    String whenAndKey = System.currentTimeMillis() + ":" + ke.getKeyChar();
                    LoggerMgr.getInstance().getKeyLogQueue().add(whenAndKey);
                }

                @Override
                public void keyPressed(KeyEvent ke) {

                }

                @Override
                public void keyReleased(KeyEvent ke) {

                }
            };

            JTextField nameField = new JTextField(19);
            nameField.addKeyListener(keyListener);

            JTextArea questionArea = new JTextArea(3,20);
            questionArea.addKeyListener(keyListener);

            JPanel rootPanel = new JPanel();
            rootPanel.setLayout(new BorderLayout());
            JPanel north = new JPanel();
            JPanel south = new JPanel();

            north.add(new JLabel("File name:"));
            north.add(nameField);

            south.add(new JLabel("Question:"));
            south.add(questionArea);

            rootPanel.add(north, BorderLayout.NORTH);
            rootPanel.add(south, BorderLayout.SOUTH);

            int result = JOptionPane.showConfirmDialog(null, rootPanel,
                    "New File", JOptionPane.OK_CANCEL_OPTION);

            if (result == JOptionPane.OK_OPTION) {
                currentQuestionOptional = Optional.of(new SimplifyPiQuestion(nameField.getText() + ".spq",
                        questionArea.getText(), ""));
                getLabelQuestion().setText(currentQuestionOptional.isPresent() ?
                        currentQuestionOptional.get().getQuestion() : "");
                setStateAndUpdateUserInterface(State.CLEAN);

            }
        });

        panelButtons.setLayout(new FlowLayout());

        panelButtons.add(buttonNew);
        panelButtons.add(buttonSave);
        panelButtons.add(buttonClose);

        panelSouth = new JPanel();
        panelSouth.setLayout(new BorderLayout());
        panelSouth.add(panelButtons, BorderLayout.NORTH);
        labelStatus = new JLabel(" ", SwingConstants.RIGHT);
        labelStatus.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 10));
        labelStatus.setOpaque(true);
        panelSouth.add(labelStatus, BorderLayout.CENTER);

        labelCleanDirty = new JLabel(" ", SwingConstants.LEFT);
        labelCleanDirty.setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 2));
        labelCleanDirty.setOpaque(true);
        panelSouth.add(labelCleanDirty, BorderLayout.SOUTH);

        getContentPane().add(panelSouth, BorderLayout.SOUTH);

        setTitle("SimplifyPi Notepad");
        //set the size of the window
        setSize(300,300);

        //center window
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int adjustX = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int adjustY = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(adjustX, adjustY);
        setStateAndUpdateUserInterface(State.EMPTY);
        setVisible(true);
    }






    @Override
    //Overridden so we can exit when window is closed
    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            System.exit(0);
        }
    }




    //KeyListener interface contract methods. All three are required
    @Override
    public void keyTyped(KeyEvent e) {
        //add key to keyLogQueue
        String whenAndKey = System.currentTimeMillis() + ":" + e.getKeyChar();
        LoggerMgr.getInstance().getKeyLogQueue().add(whenAndKey);

        //update the model
         if (currentQuestionOptional.isPresent()){
             currentQuestionOptional.get().setAnswer(answerText.getText());
             setStateAndUpdateUserInterface(State.DIRTY);
         }



    }

    //stub to satisfy the KeyListener interface
    @Override
    public void keyPressed(KeyEvent e) { }

    //stub to satisfy the KeyListener interface
    @Override
    public void keyReleased(KeyEvent e) { }
}
