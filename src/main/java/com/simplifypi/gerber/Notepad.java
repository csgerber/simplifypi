package com.simplifypi.gerber;

import java.awt.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;


public class Notepad implements Runnable {


    public static final int MILLIS_BETWEEN_CHECKS = 5_000;
    private final WindowFrame windowFrame;
    private final AtomicInteger blackCount = new AtomicInteger();

    private final Thread backgroundThread;
    private boolean tenSecondFlag;

    //we can easily externalize this set to a RESTful service or persist it to a file. However,
    //for the purposes of this simple sandbox application we use in-memory. We use HashSet to leverage
    // continuous O(1) seek times
    private static final Set<String> blackList;
    static {
        blackList = new HashSet<>();
        blackList.add("Red Dawn ransomeware");
        blackList.add("Keystroke logger");
        blackList.add("Zero Day malware");
        //blackList.add("_usbmuxd");
    }

    public static void main(String args[]) {
        //typical Swing application start; we pass EventQueue a Runnable object.
        EventQueue.invokeLater(Notepad::new);
    }

    public Notepad() {

        windowFrame = new WindowFrame();

        //fire up the background thread
        backgroundThread = new Thread(this);
        backgroundThread.start();

    }


    // Notepad implements runnable, and must have run method
    @Override
    public void run() {

        // lower background thread priority, thereby yielding to the "main" aka 'Swing Event Dispatch' thread
        // which listens to key events. This will keep the UI responsive
        backgroundThread.setPriority(Thread.MIN_PRIORITY);
        while (Thread.currentThread() == backgroundThread) {

            blackCount.set(0);
            tenSecondFlag = !tenSecondFlag;
            BiConsumer<ProcessHandle, Set<String>> processHandleSetBiConsumer = (ph, set) -> {

                //check blacklisted apps every 5 seconds
                if (set.contains(stringOrElse(ph.info().user()))){
                    blackCount.incrementAndGet();
                }
                //only log every 10 seconds
                if (tenSecondFlag) {
                    LoggerMgr.getInstance().getLogger().info(lineItemDetail(ph));
                }

            };

            LoggerMgr.getInstance().getLogger().info("PROCESSES AT: " + System.currentTimeMillis());
            //stream every 5 seconds.
            ProcessHandle.allProcesses()
                    .forEach(ph -> processHandleSetBiConsumer.accept(ph, blackList));

            //check blacklisted apps every 5 seconds
            if (blackCount.get() > 0){
                windowFrame.getLabelStatus().setBackground(Color.RED);
                windowFrame.getLabelStatus().setText("Status: Blacklisted app is running");
            } else {
                windowFrame.getLabelStatus().setBackground(Color.GREEN);
                windowFrame.getLabelStatus().setText("Status: Safe");
            }

           //purge the keyLogging queue every 10 seconds
            if (tenSecondFlag) {
                LoggerMgr.getInstance().getLogger().info("KEYSTROKES");
                while (!LoggerMgr.getInstance().getKeyLogQueue().isEmpty()) {
                    LoggerMgr.getInstance().getLogger().info((LoggerMgr.getInstance().getKeyLogQueue().remove()));
                }

               if ( windowFrame.getSpqState() != WindowFrame.State.EMPTY) {
                   SerializeMgr.getInstance().writeSpq(windowFrame.getCurrentQuestionOptional().orElseThrow());
                   windowFrame.setStateAndUpdateUserInterface(WindowFrame.State.CLEAN);
               }

            }


            try {
                Thread.sleep(MILLIS_BETWEEN_CHECKS);
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }


    }

    private static String lineItemDetail(ProcessHandle process) {
        return String.format("%8d %8s %10s %26s %-40s",
                process.pid(),
                stringOrElse(process.parent().map(ProcessHandle::pid)),
                stringOrElse(process.info().user()),
                stringOrElse(process.info().startInstant()),
                stringOrElse(process.info().commandLine()));
    }

    private static String stringOrElse(Optional<?> optional) {
        return optional.map(Object::toString).orElse("-");
    }


}
