package com.simplifypi.gerber;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

public class SerializeMgr {

    private static SerializeMgr instance;

    private SerializeMgr() { }

    public static SerializeMgr getInstance() {
        if (null == instance){
            instance = new SerializeMgr();
        }
        return instance;
    }



    public void  writeSpq(SimplifyPiQuestion simplifyPiQuestion)  {

        XMLEncoder encoder = null;
        try {
            encoder = new XMLEncoder(new BufferedOutputStream(
                    new FileOutputStream(simplifyPiQuestion.getFileName())));
            encoder.writeObject(simplifyPiQuestion);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            encoder.close();
        }



    }

    public void printXml(SimplifyPiQuestion simplifyPiQuestion) throws IOException {

        String strRet = "";
        BufferedReader br = new BufferedReader(new FileReader(new File(simplifyPiQuestion.getFileName())));
        String strLine = "";

        while ((strLine = br.readLine()) != null) {
            strRet += strLine + "\n";
        }
        System.out.println(strRet);
    }

    public SimplifyPiQuestion getPersistedQuestion(SimplifyPiQuestion simplifyPiQuestion) throws FileNotFoundException {

        XMLDecoder decoder = new XMLDecoder(new FileInputStream(simplifyPiQuestion.getFileName()));
        SimplifyPiQuestion spq = null;
        try {
            spq = (SimplifyPiQuestion) decoder.readObject();
        } catch (ArrayIndexOutOfBoundsException e) {
            spq = new SimplifyPiQuestion("", "", "");
        } finally {
            decoder.close();
        }


        return spq;
    }


}
