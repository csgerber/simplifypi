package com.simplifypi.gerber;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class LoggerMgr {

    public static final String LOGFILE_LOG = "./logfile.log";
    private final Queue<String> keyLogQueue = new LinkedList<>();
    private final Logger logger = Logger.getLogger(LoggerMgr.class.getName());


    public Queue<String> getKeyLogQueue() {
        return keyLogQueue;
    }

    private static LoggerMgr instance = null;

    private LoggerMgr() {

        try {
            Handler fileHandler = new FileHandler(LOGFILE_LOG);
            SimpleFormatter simple = new SimpleFormatter();
            fileHandler.setFormatter(simple);
            logger.addHandler(fileHandler);

        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public static LoggerMgr getInstance(){
        if (null == instance){
            instance = new LoggerMgr();
        }
        return instance;
    }
    public Logger getLogger(){
        return logger;
    }



}
