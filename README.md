Sandbox application for interview with SimplifyPI

Thanks for the opportunity to interview with SimplifyPI. I chose to use Java Swing because it affords low-level 
access to Java threads, transparent in its implementation, and doesn't require additional resources such as inflatable 
xml layout files.  

I'm using the Lombok library to  provide automatic getters/setters for certain classes. 

I was travelling this weekend and ran out of time. All features implemented except for: 1/ the prompt when data is 
dirty prior to close, and 2/ recover from crash by loading last good file. 

I look forward to discussing the app. 
